cmake_minimum_required(VERSION 3.26)
project(Uebung02)

set(CMAKE_CXX_STANDARD 14)

add_executable(Uebung02
        src/main.c
        src/log2.c
        src/log2.h
        src/bit_mods.c
        src/bit_mods.h
        src/bit_mods_macros.h
        src/mp2723.c
        src/mp2723.h
)

# Fetch GoogleTest
Include(FetchContent)
FetchContent_Declare(
        googletest
        GIT_REPOSITORY https://github.com/google/googletest.git
        GIT_TAG        v1.15.2
)
set(gtest_force_shared_crt ON CACHE BOOL "" FORCE)
FetchContent_MakeAvailable(googletest)


# Build Tests
enable_testing()

add_executable(test_log
        src/log2.c
        src/log2.h
        src/log.test.cpp
)
add_executable(test_bitmods
        src/bit_mods.c
        src/bit_mods.h
        src/bit_mods.test.cpp
)
add_executable(test_bitmods_macros
        src/bit_mods_macros.h
        src/bit_mods_macros.test.cpp
)
add_executable(test_mp2723
        src/mp2723.c
        src/mp2723.h
        src/mp2723.test.cpp
)

target_link_libraries(test_log PRIVATE GTest::gtest_main)
target_link_libraries(test_bitmods PRIVATE GTest::gtest_main)
target_link_libraries(test_bitmods_macros PRIVATE GTest::gtest_main)
target_link_libraries(test_mp2723 PRIVATE GTest::gtest_main)
target_include_directories(test_log PRIVATE src/)
target_include_directories(test_bitmods PRIVATE src/)
target_include_directories(test_bitmods_macros PRIVATE src/)
target_include_directories(test_mp2723 PRIVATE src/)
include(GoogleTest)
gtest_discover_tests(test_log)
gtest_discover_tests(test_bitmods)
gtest_discover_tests(test_bitmods_macros)
gtest_discover_tests(test_mp2723)
