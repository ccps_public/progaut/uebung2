extern "C" {
#include "bit_mods_macros.h"
}
#include <gtest/gtest.h>

TEST(bit_mods_macros, read_bit) {
    EXPECT_TRUE(READ_BIT(0b1011011, 0));
    EXPECT_FALSE(READ_BIT(0b1011010, 0));
    EXPECT_TRUE(READ_BIT(0b1011011, 1));
    EXPECT_FALSE(READ_BIT(0b1011011, 2));
    EXPECT_FALSE(READ_BIT(0b1011011, 5));
    EXPECT_TRUE(READ_BIT(0b1011011, 6));

    EXPECT_TRUE(READ_BIT(0xA1234567, 31));
    EXPECT_FALSE(READ_BIT(0x71234567, 31));

    // check if macro returns exactly "1" for "true"
    EXPECT_EQ(READ_BIT(0b1011011, 6), 1);
}

TEST(bit_mods_macros, read_bit_args) {

    EXPECT_FALSE(READ_BIT(0b11111011, 1 << 1));
    EXPECT_TRUE(READ_BIT(0b00000100, 1 << 1));

    EXPECT_FALSE(READ_BIT(0b11111011 << 1, 3));
    EXPECT_TRUE(READ_BIT(0b00000100 << 1, 3));
}

TEST(bit_mods_macros, set_bit) {
    uint32_t reg;

    reg = 0b0;
    SET_BIT(reg, 0);
    EXPECT_EQ(reg, 0b1);

    reg = 0b1;
    SET_BIT(reg, 0);
    EXPECT_EQ(reg, 0b1);

    reg = 0b0;
    SET_BIT(reg, 1);
    EXPECT_EQ(reg, 0b10);

    reg = 0b0;
    SET_BIT(reg, 3);
    EXPECT_EQ(reg, 0b1000);

    reg = 0b0001;
    SET_BIT(reg, 3);
    EXPECT_EQ(reg, 0b1001);

    reg = 0xA1234567;
    SET_BIT(reg, 31);
    EXPECT_EQ(reg, 0xA1234567);

    reg = 0x71234567;
    SET_BIT(reg, 31);
    EXPECT_EQ(reg, 0xF1234567);


    reg = 0b0;
    SET_BIT(reg, 0);
    EXPECT_EQ(reg, 0b1);

    reg = 0b0;
    SET_BIT(reg, 1);
    EXPECT_EQ(reg, 0b10);

    reg = 0b0;
    SET_BIT(reg, 3);
    EXPECT_EQ(reg, 0b1000);

    reg = 0b0001;
    SET_BIT(reg, 3);
    EXPECT_EQ(reg, 0b1001);
}

TEST(bit_mods_macros, set_bit_args) {
    uint32_t reg;

    reg = 0b11101111;
    SET_BIT(reg, 1 << 2);
    EXPECT_EQ(reg, 0xFF);
}

TEST(bit_mods_macros, clear_bit) {
    uint32_t reg;

    reg = 0b1;
    CLEAR_BIT(reg, 0);
    EXPECT_EQ(reg, 0b0);
    reg = 0b11;
    CLEAR_BIT(reg, 1);
    EXPECT_EQ(reg, 0b01);
    reg = 0b1111;
    CLEAR_BIT(reg, 2);
    EXPECT_EQ(reg, 0b1011);
    reg = 0b1111;
    CLEAR_BIT(reg, 3);
    EXPECT_EQ(reg, 0b0111);

    reg = 0xA1234567;
    CLEAR_BIT(reg, 31);
    EXPECT_EQ(reg, 0x21234567);
    reg = 0x71234567;
    CLEAR_BIT(reg, 31);
    EXPECT_EQ(reg, 0x71234567);
}

TEST(bit_mods_macros, clear_bit_args) {
    uint32_t reg;

    reg = 0b00010000;
    CLEAR_BIT(reg, 1 << 2);
    EXPECT_EQ(reg, 0);
}

TEST(bit_mods_macros, toggle_bit) {
    uint32_t reg;

    reg = 0b1;
    TOGGLE_BIT(reg, 0);
    EXPECT_EQ(reg, 0b0);
    reg = 0b0;
    TOGGLE_BIT(reg, 0);
    EXPECT_EQ(reg, 0b1);
    reg = 0b11;
    TOGGLE_BIT(reg, 1);
    EXPECT_EQ(reg, 0b01);
    reg = 0b00;
    TOGGLE_BIT(reg, 1);
    EXPECT_EQ(reg, 0b10);
    reg = 0b10;
    TOGGLE_BIT(reg, 1);
    EXPECT_EQ(reg, 0b00);
    reg = 0b01;
    TOGGLE_BIT(reg, 1);
    EXPECT_EQ(reg, 0b11);
    reg = 0b1111;
    TOGGLE_BIT(reg, 2);
    EXPECT_EQ(reg, 0b1011);
    reg = 0b1011;
    TOGGLE_BIT(reg, 2);
    EXPECT_EQ(reg, 0b1111);
    reg = 0b10101011;
    TOGGLE_BIT(reg, 3);
    EXPECT_EQ(reg, 0b10100011);
    reg = 0b10100011;
    TOGGLE_BIT(reg, 3);
    EXPECT_EQ(reg, 0b10101011);

    reg = 0xA1234567;
    TOGGLE_BIT(reg, 31);
    EXPECT_EQ(reg, 0x21234567);
    reg = 0x71234567;
    TOGGLE_BIT(reg, 31);
    EXPECT_EQ(reg, 0xF1234567);
}

TEST(bit_mods_macros, toggle_bit_args) {
    uint32_t reg;

    reg = 0b00010000;
    TOGGLE_BIT(reg, 1 << 2);
    EXPECT_EQ(reg, 0);

    reg = 0b11101111;
    TOGGLE_BIT(reg, 1 << 2);
    EXPECT_EQ(reg, 0xFF);
}

TEST(bit_mods_macros, assign_bit) {
    uint32_t reg;

    reg = 0b0;
    ASSIGN_BIT(reg, 0, true);
    EXPECT_EQ(reg, 0b1);

    // check that each value != 0 is accepted as true
    reg = 0b0;
    ASSIGN_BIT(reg, 0, 0xF4);
    EXPECT_EQ(reg, 0b1);

    reg = 0b0;
    ASSIGN_BIT(reg, 0, false);
    EXPECT_EQ(reg, 0b0);
    reg = 0b1;
    ASSIGN_BIT(reg, 0, true);
    EXPECT_EQ(reg, 0b1);
    reg = 0b1;
    ASSIGN_BIT(reg, 0, false);
    EXPECT_EQ(reg, 0b0);

    reg = 0b10;
    ASSIGN_BIT(reg, 0, true);
    EXPECT_EQ(reg, 0b11);
    reg = 0b10;
    ASSIGN_BIT(reg, 0, false);
    EXPECT_EQ(reg, 0b10);
    reg = 0b11;
    ASSIGN_BIT(reg, 0, true);
    EXPECT_EQ(reg, 0b11);
    reg = 0b11;
    ASSIGN_BIT(reg, 0, false);
    EXPECT_EQ(reg, 0b10);

    reg = 0b10101011;
    ASSIGN_BIT(reg, 3, true);
    EXPECT_EQ(reg, 0b10101011);
    reg = 0b10101011;
    ASSIGN_BIT(reg, 3, false);
    EXPECT_EQ(reg, 0b10100011);

    reg = 0xA1234567;
    ASSIGN_BIT(reg, 31, true);
    EXPECT_EQ(reg, 0xA1234567);
    reg = 0xA1234567;
    ASSIGN_BIT(reg, 31, false);
    EXPECT_EQ(reg, 0x21234567);
    reg = 0x71234567;
    ASSIGN_BIT(reg, 31, true);
    EXPECT_EQ(reg, 0xF1234567);
    reg = 0x71234567;
    ASSIGN_BIT(reg, 31, false);
    EXPECT_EQ(reg, 0x71234567);
}

TEST(bit_mods_macros, assign_bit_args) {
    uint32_t reg;
    volatile int _true = 1;
    volatile int _false = 0;

    reg = 0b00010000;
    ASSIGN_BIT(reg, 1 << 2, _true << 1 ? 0 : 1);
    EXPECT_EQ(reg, 0);

    reg = 0b00010000;
    ASSIGN_BIT(reg, 1 << 2, _false << 1 ? 1 : 0);
    EXPECT_EQ(reg, 0);

    reg = 0b11101111;
    ASSIGN_BIT(reg, 1 << 2, _true << 1 ? 1 : 0);
    EXPECT_EQ(reg, 0xFF);

    reg = 0b11101111;
    ASSIGN_BIT(reg, 1 << 2, _false << 1 ? 0 : 1);
    EXPECT_EQ(reg, 0xFF);
}
