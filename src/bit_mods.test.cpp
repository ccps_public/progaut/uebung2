extern "C" {
#include "bit_mods.h"
}
#include <gtest/gtest.h>

TEST(bit_mods, read_bit) {
    EXPECT_TRUE(readBit(0b1011011, 0));
    EXPECT_FALSE(readBit(0b1011010, 0));
    EXPECT_TRUE(readBit(0b1011011, 1));
    EXPECT_FALSE(readBit(0b1011011, 2));
    EXPECT_FALSE(readBit(0b1011011, 5));
    EXPECT_TRUE(readBit(0b1011011, 6));

    EXPECT_TRUE(readBit(0xA1234567, 31));
    EXPECT_FALSE(readBit(0x71234567, 31));

}

TEST(bit_mods, set_bit) {
    EXPECT_EQ(setBit(0b0, 0), 0b1);
    EXPECT_EQ(setBit(0b1, 0), 0b1);
    EXPECT_EQ(setBit(0b0, 1), 0b10);
    EXPECT_EQ(setBit(0b0, 3), 0b1000);
    EXPECT_EQ(setBit(0b0001, 3), 0b1001);

    EXPECT_EQ(setBit(0xA1234567, 31), 0xA1234567);
    EXPECT_EQ(setBit(0x71234567, 31), 0xF1234567);


    EXPECT_EQ(setBit(0b0, 0), 0b1);
    EXPECT_EQ(setBit(0b0, 1), 0b10);
    EXPECT_EQ(setBit(0b0, 3), 0b1000);
    EXPECT_EQ(setBit(0b0001, 3), 0b1001);
}

TEST(bit_mods, clear_bit) {
    EXPECT_EQ(clearBit(0b1, 0), 0b0);
    EXPECT_EQ(clearBit(0b11, 1), 0b01);
    EXPECT_EQ(clearBit(0b1111, 2), 0b1011);
    EXPECT_EQ(clearBit(0b1111, 3), 0b0111);

    EXPECT_EQ(clearBit(0xA1234567, 31), 0x21234567);
    EXPECT_EQ(clearBit(0x71234567, 31), 0x71234567);
}

TEST(bit_mods, toggle_bit) {
    EXPECT_EQ(toggleBit(0b1, 0), 0b0);
    EXPECT_EQ(toggleBit(0b0, 0), 0b1);
    EXPECT_EQ(toggleBit(0b11, 1), 0b01);
    EXPECT_EQ(toggleBit(0b00, 1), 0b10);
    EXPECT_EQ(toggleBit(0b10, 1), 0b00);
    EXPECT_EQ(toggleBit(0b01, 1), 0b11);
    EXPECT_EQ(toggleBit(0b1111, 2), 0b1011);
    EXPECT_EQ(toggleBit(0b1011, 2), 0b1111);
    EXPECT_EQ(toggleBit(0b10101011, 3), 0b10100011);
    EXPECT_EQ(toggleBit(0b10100011, 3), 0b10101011);

    EXPECT_EQ(toggleBit(0xA1234567, 31), 0x21234567);
    EXPECT_EQ(toggleBit(0x71234567, 31), 0xF1234567);
}

TEST(bit_mods, assign_bit) {
    EXPECT_EQ(assignBit(0b0, 0, true), 0b1);
    EXPECT_EQ(assignBit(0b0, 0, false), 0b0);
    EXPECT_EQ(assignBit(0b1, 0, true), 0b1);
    EXPECT_EQ(assignBit(0b1, 0, false), 0b0);

    EXPECT_EQ(assignBit(0b10, 0, true), 0b11);
    EXPECT_EQ(assignBit(0b10, 0, false), 0b10);
    EXPECT_EQ(assignBit(0b11, 0, true), 0b11);
    EXPECT_EQ(assignBit(0b11, 0, false), 0b10);

    EXPECT_EQ(assignBit(0b10101011, 3, true), 0b10101011);
    EXPECT_EQ(assignBit(0b10101011, 3, false), 0b10100011);

    EXPECT_EQ(assignBit(0xA1234567, 31, true), 0xA1234567);
    EXPECT_EQ(assignBit(0xA1234567, 31, false), 0x21234567);
    EXPECT_EQ(assignBit(0x71234567, 31, true), 0xF1234567);
    EXPECT_EQ(assignBit(0x71234567, 31, false), 0x71234567);
}
