#include <stdio.h>

#include "bit_mods.h"
#include "bit_mods_macros.h"
#include "log2.h"

int main(void) {
    printf("%i", clearBit(0b11, 1));
    return 0;
}
