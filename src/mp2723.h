#ifndef UEBUNG02_MP2723_H
#define UEBUNG02_MP2723_H

#include <stdint.h>

typedef enum {
    RECHARGE_THRESHOLD_100mV = 0,
    RECHARGE_THRESHOLD_200mV = 1,
} recharge_threshold_t;

recharge_threshold_t getRechargeThreshold(uint8_t reg);
uint16_t getRegVoltage_mV(uint8_t reg);
uint8_t setRechargeThreshold(uint8_t reg, recharge_threshold_t value);
uint8_t setRegVoltage_mV(uint8_t reg, uint16_t value);

#endif  // UEBUNG02_MP2723_H
