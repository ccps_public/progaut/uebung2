#ifndef UEBUNG02_LOG2_H
#define UEBUNG02_LOG2_H

int floorLog2(unsigned int n);
int ceilLog2(unsigned int n);

#endif  // UEBUNG02_LOG2_H
