extern "C" {
#include "log2.h"
}
#include <gtest/gtest.h>

#include <climits>

TEST(log2, zero) {
    EXPECT_EQ(floorLog2(0), INT_MIN);
    EXPECT_EQ(ceilLog2(0), INT_MIN);
}

TEST(log2, positiv) {
    EXPECT_EQ(floorLog2(1), 0);
    EXPECT_EQ(ceilLog2(1), 0);
    EXPECT_EQ(floorLog2(2), 1);
    EXPECT_EQ(ceilLog2(2), 1);
    EXPECT_EQ(floorLog2(4), 2);
    EXPECT_EQ(ceilLog2(4), 2);
    EXPECT_EQ(floorLog2(8), 3);
    EXPECT_EQ(ceilLog2(8), 3);
    EXPECT_EQ(floorLog2(0x80000000), 31);
    EXPECT_EQ(ceilLog2(0x80000000), 31);
}

TEST(log2, floor) {
    EXPECT_EQ(floorLog2(3), 1);
    EXPECT_EQ(floorLog2(5), 2);
    EXPECT_EQ(floorLog2(9), 3);
    EXPECT_EQ(floorLog2(INT_MAX), 30);
    EXPECT_EQ(floorLog2(0x80000001), 31);
    EXPECT_EQ(floorLog2(0xFFFFFFFF), 31);
}

TEST(log2, ceil) {
    EXPECT_EQ(ceilLog2(3), 2);
    EXPECT_EQ(ceilLog2(5), 3);
    EXPECT_EQ(ceilLog2(9), 4);
    EXPECT_EQ(ceilLog2(INT_MAX), 31);
    EXPECT_EQ(ceilLog2(0x80000001), 32);
    EXPECT_EQ(ceilLog2(0xFFFFFFFF), 32);
}