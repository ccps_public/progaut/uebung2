#ifndef UEBUNG02_BIT_MODS_H
#define UEBUNG02_BIT_MODS_H

#include <stdbool.h>
#include <stdint.h>

bool readBit(uint32_t data, uint8_t position);
uint32_t setBit(uint32_t data, uint8_t position);
uint32_t clearBit(uint32_t data, uint8_t position);
uint32_t toggleBit(uint32_t data, uint8_t position);
uint32_t assignBit(uint32_t data, uint8_t position, bool value);

#endif  // UEBUNG02_BIT_MODS_H
