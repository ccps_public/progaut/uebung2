extern "C" {
#include "mp2723.h"
}
#include <gtest/gtest.h>

TEST(mp2723, getRechargeThreshold) {
    EXPECT_EQ(getRechargeThreshold(0b00000000), RECHARGE_THRESHOLD_100mV);
    EXPECT_EQ(getRechargeThreshold(0b00000001), RECHARGE_THRESHOLD_200mV);
    EXPECT_EQ(getRechargeThreshold(0b01100000), RECHARGE_THRESHOLD_100mV);
    EXPECT_EQ(getRechargeThreshold(0b01100001), RECHARGE_THRESHOLD_200mV);
    EXPECT_EQ(getRechargeThreshold(0b11111110), RECHARGE_THRESHOLD_100mV);
    EXPECT_EQ(getRechargeThreshold(0b11111111), RECHARGE_THRESHOLD_200mV);
    EXPECT_EQ(getRechargeThreshold(0b10011000), RECHARGE_THRESHOLD_100mV);
    EXPECT_EQ(getRechargeThreshold(0b10011001), RECHARGE_THRESHOLD_200mV);
}

TEST(mp2723, getRegVoltage_mV) {
    EXPECT_EQ(getRegVoltage_mV(0b00000000), 3400);
    EXPECT_EQ(getRegVoltage_mV(0b10000001), 4040);
    EXPECT_EQ(getRegVoltage_mV(0b11111111), 4670);
}

TEST(mp2723, setRechargeThreshold) {
    EXPECT_EQ(setRechargeThreshold(0b00000001, RECHARGE_THRESHOLD_100mV),
              0b00000000);
    EXPECT_EQ(setRechargeThreshold(0b00000000, RECHARGE_THRESHOLD_200mV),
              0b00000001);
    EXPECT_EQ(setRechargeThreshold(0b11111111, RECHARGE_THRESHOLD_100mV),
              0b11111110);
    EXPECT_EQ(setRechargeThreshold(0b11111110, RECHARGE_THRESHOLD_200mV),
              0b11111111);
    EXPECT_EQ(setRechargeThreshold(0b11111110, RECHARGE_THRESHOLD_100mV),
              0b11111110);
    EXPECT_EQ(setRechargeThreshold(0b11111111, RECHARGE_THRESHOLD_200mV),
              0b11111111);
}

TEST(mp2723, setRegVoltage_mV_exact) {
    EXPECT_EQ(setRegVoltage_mV(0b11111111, 3400), 0b00000001);
    EXPECT_EQ(setRegVoltage_mV(0b11111111, 4040), 0b10000001);
    EXPECT_EQ(setRegVoltage_mV(0b11111111, 4070), 0b10000111);
    EXPECT_EQ(setRegVoltage_mV(0b11111110, 4070), 0b10000110);
    EXPECT_EQ(setRegVoltage_mV(0b11111110, 4670), 0b11111110);
}

TEST(mp2723, setRegVoltage_mV_round) {
    EXPECT_EQ(setRegVoltage_mV(0b11111111, 3404), 0b00000001);
    EXPECT_EQ(setRegVoltage_mV(0b11111111, 3405), 0b00000011);
    EXPECT_EQ(setRegVoltage_mV(0b11111111, 4035), 0b10000001);
    EXPECT_EQ(setRegVoltage_mV(0b11111111, 4034), 0b01111111);
    EXPECT_EQ(setRegVoltage_mV(0b11111111, 4052), 0b10000011);
    EXPECT_EQ(setRegVoltage_mV(0b11111111, 4664), 0b11111101);
    EXPECT_EQ(setRegVoltage_mV(0b11111111, 4665), 0b11111111);
}

TEST(mp2723, setRegVoltage_mV_limit) {
    EXPECT_EQ(setRegVoltage_mV(0b11111111, 3399), 0b00000001);
    EXPECT_EQ(setRegVoltage_mV(0b11111111, 0), 0b00000001);
    EXPECT_EQ(setRegVoltage_mV(0b11111111, 4671), 0b11111111);
    EXPECT_EQ(setRegVoltage_mV(0b11111111, 0xFFFF), 0b11111111);
}
